/*
 * Watershed.cpp
 *
 *  Created on: 28/05/2015
 *      Author: adam
 */

#include "BaseCV.h"
#include <iostream>
#include <stdio.h>
#include <string>

using namespace cv;
using namespace std;

class Watershed
{

public:

	void ExtractExudates(Mat& procImage, Mat& originalImage, Mat& opticalMask,  Point2f opticalDiscPoint, Mat& finalResult, vector<Mat>& processes) {


		Mat mask;
		mask = imread("Training/fmask.tif", CV_LOAD_IMAGE_COLOR);
		cvtColor(mask, mask, CV_RGB2GRAY);
		Size resizeSize((int)mask.cols/3, (int)mask.rows/3);
		resize(mask, mask,resizeSize, CV_INTER_CUBIC);

		Mat processedImage;
		WatershedPreprocessing(procImage,processedImage);

		Mat operatedImage;
		MaskOpticalDisc(processedImage,opticalMask,operatedImage);
		GaussianBlur(operatedImage,operatedImage, Size(3,3),0,0);
		//imshow("Operation Image", operatedImage);

		Mat result;
		ThresholdPotentials(processedImage, mask, result,opticalDiscPoint );
		processes.push_back(result);

		Mat finerSeeds;
		FineSeeds(result, processedImage, finerSeeds, mask);
		processes.push_back(finerSeeds);

		Mat watershedExudates;
		WatershedOperation(originalImage, result, finerSeeds,watershedExudates);
		processes.push_back(watershedExudates);

		Mat exudateMask;
		GenerateWatershedMask(watershedExudates,exudateMask );

		finalResult = exudateMask;


	}




private:

	void GenerateWatershedMask(Mat& watershedImage, Mat& exudateThreshold) {

		Mat exudateMask;
		watershedImage.copyTo(exudateMask);

		threshold(exudateMask, exudateMask, 250,255, CV_THRESH_BINARY);

		//imshow("Exudate Mask", exudateMask);

		exudateThreshold = exudateMask;

	}


	void ThresholdPotentials(Mat& image, Mat& mask, Mat& result, Point2f opticalDiscPoint) {

		Mat whiteBack;
		image.copyTo(whiteBack);
		whiteBack.setTo(190);

		image.copyTo(whiteBack, mask);

		Mat stucturingEllipse = getStructuringElement(1, Size(2, 2));
		//morphologyEx(whiteBack, whiteBack, MORPH_DILATE, stucturingEllipse, Point(-1,-1),1);
		//imshow("Back Filtered",whiteBack);


		Mat otsu;
		threshold(whiteBack, otsu, 200,255, CV_THRESH_BINARY|CV_THRESH_OTSU);
		//imshow("Otsu", otsu);

		morphologyEx(otsu, otsu, MORPH_CLOSE, stucturingEllipse, Point(-1,-1),1);
		//imshow("Closing", otsu);

		Mat opticalCloud;
		ExtractOpticalCloud (otsu, opticalCloud,opticalDiscPoint);

		Mat finalExudateCandidates;
		otsu.copyTo(finalExudateCandidates);
		morphologyEx(opticalCloud, opticalCloud, MORPH_DILATE, stucturingEllipse, Point(-1,-1),4);

		subtract(finalExudateCandidates,opticalCloud,finalExudateCandidates);
		//morphologyEx(finalExudateCandidates, finalExudateCandidates, MORPH_ERODE, stucturingEllipse, Point(-1,-1),3);

		//imshow("Candidates",finalExudateCandidates );

		result = finalExudateCandidates;

	}

	void WatershedOperation(Mat& realImage, Mat& externalMarkers, Mat& internalMarkers, Mat& result) {

		Mat waterShedImage;
		realImage.copyTo(waterShedImage);
		Size resizeSize((int)waterShedImage.cols/3, (int)waterShedImage.rows/3);
		resize(waterShedImage, waterShedImage,resizeSize, CV_INTER_CUBIC);


		Mat external;
		threshold(externalMarkers,external,1,128,1);
		//imshow("External", external);

		Mat marker;
		add(external, internalMarkers, marker);
		//imshow("Marker", marker);


		Mat im32;
		marker.convertTo(im32, CV_32S);

		watershed(waterShedImage, im32);
		Mat m;
		convertScaleAbs(im32, m);

		//imshow("M",m);

		result = m;


	}

	void FineSeeds(Mat& backgroundMarkers, Mat& image, Mat& fineSeeds, Mat& mask) {

		Mat output;
		KirschOperator(image, output);
		//imshow("Kirsh", output);

		Mat threshKirsh;
		threshold(output, threshKirsh,210, 255, THRESH_BINARY);

		morphologyEx(threshKirsh, threshKirsh, MORPH_ERODE, getStructuringElement(1, Size(2, 2)), Point(-1,-1),1);

		bitwise_and(backgroundMarkers, threshKirsh, threshKirsh);
		bitwise_and(threshKirsh, mask, threshKirsh);

		fineSeeds = threshKirsh;

	}

	void WatershedPreprocessing(Mat& image, Mat& processedImage) {

		Mat morphImage;
		image.copyTo(morphImage);

		Size convertSize((int)morphImage.cols/3, (int)morphImage.rows/3);
		resize(morphImage, morphImage,convertSize, CV_INTER_CUBIC);

		Mat stucturingEllipse = getStructuringElement(1, Size(8, 8));

		morphologyEx(morphImage, morphImage, MORPH_CLOSE, stucturingEllipse, Point(-1,-1),1);

		processedImage = morphImage;



	}

	void KirschOperator(Mat& image, Mat& output) {

		int kernalSize = 3;

		Mat operatedImage;
		image.copyTo(operatedImage);

		Mat kirschKernal = Mat(kernalSize,kernalSize, CV_32F);

		kirschKernal.at<float>(0,0) = 5;
		kirschKernal.at<float>(0,1) = -3;
		kirschKernal.at<float>(0,2) = -3;
		kirschKernal.at<float>(1,0) = 5;
		kirschKernal.at<float>(1,1) = 0;
		kirschKernal.at<float>(1,2) = -3;
		kirschKernal.at<float>(0,0) = 5;
		kirschKernal.at<float>(0,1) = -3;
		kirschKernal.at<float>(0,2) = -3;

		Mat kirschKernalResult;
		filter2D(operatedImage, kirschKernalResult,-1,kirschKernal);
		output = kirschKernalResult;

	}

	void MaskOpticalDisc(Mat& image, Mat& opticalMask, Mat& resultImage) {

		Mat avgVal;
		image.copyTo(avgVal);
		Scalar avgIntensity = mean(avgVal);

		Mat maskOverlay(opticalMask.size(), CV_8UC1,Scalar(0,0,0,0));
		maskOverlay.setTo(Scalar(avgIntensity[0],avgIntensity[0],avgIntensity[0]),opticalMask);

		Mat editedImage;
		subtract(image, opticalMask, editedImage);
		add(editedImage, maskOverlay, editedImage);

		resultImage = editedImage;

	}

	void ExtractOpticalCloud (Mat& blobImage, Mat& mask, Point2f opticalDiscPoint) {

		Mat image;
		blobImage.copyTo(image);

		Mat maskImage = Mat::zeros(blobImage.size(), blobImage.type());

		vector<vector<Point> > contours;
		vector<Vec4i> hierarchy;

		findContours(image, contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE, Point(0,0)); //Find the contours of the thresholded objects (look like white blobs)

		vector<vector<Point> > contoursPoly (contours.size());
		vector<Point2f> center (contours.size());
		vector<float> radius (contours.size());

		for(unsigned int i = 0; i < contours.size(); i++) {
			approxPolyDP (Mat(contours[i]), contoursPoly[i], 3, true); //Find the approximate polygonal curve using the set of contour points found

		}

		double minDis = 100000;
		double minIndex = 0;

		for(unsigned int i = 0; i < contoursPoly.size(); i++) {

			Point2f centroid = ContourCentroid(contoursPoly[i]);

			double dis = std::sqrt((centroid.x - opticalDiscPoint.x)*(centroid.x - opticalDiscPoint.x) + (centroid.y - opticalDiscPoint.y)*(centroid.y - opticalDiscPoint.y));

			if(dis < minDis) {
				minDis = dis;
				minIndex = i;
			}
		}

		Mat maskBase = Mat::zeros(image.rows, image.cols, CV_8UC1);
		vector<Point> maskContour = contoursPoly[minIndex];
		const Point* contourPoints[1] = { &maskContour[0] };
		int numPoints = (int)maskContour.size();
		fillPoly(maskBase,contourPoints,&numPoints,1, Scalar(255,255,255), 8 );

		//imshow("Optical Cloud", maskBase);

		mask = maskBase;


	}

	Point2f ContourCentroid(vector<Point> contours) {

		Point center;

		Moments moment;
		moment = moments(contours,false);

		center = Point2f(moment.m10/moment.m00,moment.m01/moment.m00);

		return center;


	}




};




