/*
 * preprocess.cpp
 *
 *  Created on: May 10, 2015
 *      Author: nuttymtv1
 */


#include "BaseCV.h"
#include "Preprocessor.h"
#include <iostream>
#include <stdio.h>
#include <string>

using namespace cv;
using namespace std;

const int GREENCHANNEL = 0;
const int GREYCHANNEL = 1;
const int HSI = 2;

const int STRUCTURE_SIZE_1 = 10;
const int STRUCTURE_SIZE_2 = 5;


struct Area {

	bool operator() (const vector<Point> &a, const vector<Point> &b) {
		return contourArea(a, false) > contourArea(b, false);
	}

};

struct Distance {

	bool operator() (const vector<double> &disA , const vector<double> &disB){
		return disA[2] > disB[2];
	}
};

Preprocessor::Preprocessor(int colorMethod) {

	colorSpaceMethod = colorMethod;

}


void Preprocessor::ImageCorrection(Mat& image) {

	Mat correctionImage;
	image.copyTo(correctionImage);

	Mat avgVal;
	cvtColor(image, avgVal, CV_RGB2GRAY);
	Scalar avgIntensity = mean(avgVal);


	int CLAHEVal = 4;
	if(avgIntensity[0] < 21.0) {
		CLAHEVal = 10;
	}


	Mat colorCorrectedImage;

	if(colorSpaceMethod == GREENCHANNEL) {
		vector<Mat> RGB;
		split(correctionImage, RGB);
		RGB[1].copyTo(colorCorrectedImage);
	} else {
		cvtColor(correctionImage, colorCorrectedImage, CV_RGB2GRAY);
	}

	medianBlur(colorCorrectedImage, colorCorrectedImage, 3);

	Ptr<CLAHE> clahe = createCLAHE();
	clahe->setClipLimit(CLAHEVal);
	clahe->apply(colorCorrectedImage, colorCorrectedImage);

	image = colorCorrectedImage;

}

void Preprocessor::OpticalDiscSegmentation(Mat& image, Mat& mask,  cv::Point2f& center) {

	Mat stucturingEllipse = getStructuringElement(1, Size(STRUCTURE_SIZE_1, STRUCTURE_SIZE_1));
	Mat stucturingEllipseDilate = getStructuringElement(1, Size(STRUCTURE_SIZE_2, STRUCTURE_SIZE_2));
	int erodeCount;
	int dilatIter;
	bool useErode;
	bool defultDilator;
	int methodFlag=0;
	vector<vector< double > > outputAreaVector;
	vector<Point2f> centroids;
	vector<Mat> maskResults;
	Point2f mean_center;

	for(int i =0 ; i < 22 ; i++){

		if(methodFlag == 0){
			useErode  = false;
			defultDilator = true;

		}else if(methodFlag == 1){
			useErode  = false;
			defultDilator = false;

		}else if(methodFlag == 2){
			useErode  = true;
			defultDilator = true;

		}else{
			useErode  = true;
			defultDilator = false;
		}



		if(useErode == false){
			erodeCount = 0;
		}

		if (defultDilator == true){
			dilatIter = 1;
		}
		else{
			dilatIter = 9;
		}

		Mat segImage;
		OpticalDiscMorphology(image, segImage, dilatIter); //Dilation: Default or Large

		Size debugSize((int)segImage.cols/3, (int)segImage.rows/3);
		resize(segImage, segImage,debugSize, CV_INTER_CUBIC);

		Mat retinaMask;
		Mat retina;

		ExtractRetinaRegion(segImage,retina, retinaMask);

		Mat opticalCandidates;

		if( i == 2 ){
			erodeCount = 5;
		}
		SegmentOpticalCandidates(retina, retinaMask, opticalCandidates , erodeCount); //0 || 5..15 Erode Value

		Mat opticalDiscMask;
		Mat drawing;

		OpticalDiscExtraction(outputAreaVector,centroids,opticalCandidates,opticalDiscMask,drawing); //Returns Area and Compactness
		maskResults.push_back(opticalDiscMask);

		if(useErode == true )
			erodeCount++;

		if(erodeCount == 15 ){
			erodeCount = 5;
		}

		if(i == 0 || i == 1 || i == 2 || i == 11){
			methodFlag++;
		}
	}

	int resultIndex = 0;
	resultIndex = SortOpticalDiscData(outputAreaVector , centroids);

	mask = maskResults[resultIndex];

	center = centroids[resultIndex];

}

void Preprocessor::OpticalDiscMorphology(Mat& image, Mat& output, int dilateIter) {


		Mat stucturingEllipse = getStructuringElement(1, Size(STRUCTURE_SIZE_1, STRUCTURE_SIZE_1));

		Mat morphImage;
		int closeIter = 2;

		image.copyTo(morphImage);
		Mat Dilat,erod;

		morphologyEx(image, morphImage, MORPH_CLOSE, stucturingEllipse, Point(-1,-1),closeIter);

		dilate(morphImage, Dilat, Mat(), Point(-1, -1), dilateIter, 1, 1);

		output = Dilat;
}

void Preprocessor::ExtractRetinaRegion(cv::Mat& image,cv::Mat& Retina, cv::Mat& mask) {

	Mat stucturingEllipse = getStructuringElement(1, Size(STRUCTURE_SIZE_2, STRUCTURE_SIZE_2));

	Mat segImage;
	image.copyTo(segImage);
	Mat threshSegImage;
	adaptiveThreshold(segImage, threshSegImage,255,ADAPTIVE_THRESH_GAUSSIAN_C, THRESH_BINARY,7,1);

	Mat circleOut;
	Mat output;

	threshSegImage.copyTo(circleOut);
	segImage.copyTo(output);
	cvtColor(output, output, CV_GRAY2RGB);

	vector<vector<Point> > contours;
	vector<Vec4i> hierarchy;

	findContours(circleOut, contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE, Point(0,0)); //Find the contours of the thresholded objects (look like white blobs)

	vector<vector<Point> > contoursPoly (contours.size());
	vector<Point2f> center (contours.size());
	vector<float> radius (contours.size());

	for(unsigned int i = 0; i < contours.size(); i++) {
		approxPolyDP (Mat(contours[i]), contoursPoly[i], 3, true); //Find the approximate polygonal curve using the set of contour points found
		minEnclosingCircle((Mat)contoursPoly[i], center[i], radius[i]);
	}

	int maxRadius = 0;
	int maxRadiusIndex;

	for(unsigned int i = 0; i < radius.size() ; i++) {
		if(radius[i] > maxRadius ) {
			maxRadius = radius[i];
			maxRadiusIndex = i;
		}
	}

	Mat whiteBack;
	output.copyTo(whiteBack);
	whiteBack.setTo(255);

	Mat circleMask = Mat::zeros(output.rows, output.cols, CV_8UC1);
	circle(circleMask,  center[maxRadiusIndex], (int)radius[maxRadiusIndex], Scalar(255,255,255), -1, 8, 0 );
	output.copyTo(whiteBack, circleMask);
	cvtColor(whiteBack, whiteBack, CV_RGB2GRAY);

	morphologyEx(circleMask, circleMask, MORPH_ERODE, stucturingEllipse, Point(-1,-1),1);

	Retina = whiteBack;
	mask = circleMask;

	//imshow("Cut out" ,whiteBack);

}

void Preprocessor::SegmentOpticalCandidates(cv::Mat& image, cv::Mat& mask, cv::Mat& result, int erodeIter) {


	Mat stucturingEllipseDilate = getStructuringElement(1, Size(4, 4));
	Mat otsu;
	threshold(image, otsu, 200,255, CV_THRESH_BINARY|CV_THRESH_OTSU);
	morphologyEx(otsu, otsu, MORPH_DILATE, stucturingEllipseDilate, Point(-1,-1),2);
	morphologyEx(otsu, otsu, MORPH_ERODE, stucturingEllipseDilate, Point(-1,-1),erodeIter);


	Mat opticalCandidates;
	bitwise_and(otsu, otsu, opticalCandidates, mask);

	result = opticalCandidates;


}

void Preprocessor::OpticalDiscExtraction(vector<vector<double > >& outputAreaVector, vector<Point2f>& centroids, Mat& image, Mat& opticalDiskMask, Mat& debugDrawing) {

	Mat stucturingEllipseDilate = getStructuringElement(1, Size(4, 4));

	Mat possibleOpticals;
	image.copyTo(possibleOpticals);

	vector<vector<Point> > contoursOpt;
	vector<Vec4i> hierarchyOpt;

	findContours(possibleOpticals, contoursOpt, hierarchyOpt, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE, Point(0,0)); //Find the contours of the thresholded objects (look like white blobs)

	vector<vector<Point> > contoursPolyOpt (contoursOpt.size());
	vector<Point2f> centerOpt (contoursOpt.size());
	vector<float> radiusOpt (contoursOpt.size());

	for(unsigned int i = 0; i < contoursOpt.size(); i++) {
		approxPolyDP (Mat(contoursOpt[i]), contoursPolyOpt[i], 3, true); //Find the approximate polygonal curve using the set of contour points found
		minEnclosingCircle((Mat)contoursPolyOpt[i], centerOpt[i], radiusOpt[i]);
	}

	sort(contoursPolyOpt.begin(), contoursPolyOpt.end(), Area());

	double maxCompactness = 0;
	double maxArea = 0;
	int maxCompactnessIndex = 0;

	RNG rng(12345);
	Mat drawing = Mat::zeros(possibleOpticals.size(), CV_8UC3);
	for(unsigned int i = 0 ; (i < 2 && i < contoursPolyOpt.size()) ; i++) {
		Scalar color = Scalar(rng.uniform(0,255), rng.uniform(0,255), rng.uniform(0,255));
		drawContours(drawing, contoursPolyOpt,i, color, 1,8,vector<Vec4i>(), 0, Point());

		double contArea = contourArea(contoursPolyOpt[i], false);
		double contPerimeter = arcLength(contoursPolyOpt[i], true);

		double compactness = (4 * M_PI * contArea) / (contPerimeter * contPerimeter);

		if(compactness > maxCompactness) {
			maxCompactness = compactness;
			maxCompactnessIndex = i;
			maxArea = contArea;
		}

		std::ostringstream ss;
		ss << "[ ";
		ss << compactness;
		ss << " ]";
		string outputArea(ss.str());
		putText(drawing, outputArea,centerOpt[i], FONT_HERSHEY_SIMPLEX, 0.35, color, 1,8,false); //Output distance

	}

	if(contoursPolyOpt.size() > 0) {
		vector<double> areaCompacness; // keep the area and compactness for each iteration
		Mat opticalMask = Mat::zeros(image.rows, image.cols, CV_8UC1);
		vector<Point> opticalContour = contoursPolyOpt[maxCompactnessIndex];
		const Point* contourPoints[1] = { &opticalContour[0] };
		int numPoints = (int)opticalContour.size();
		fillPoly(opticalMask,contourPoints,&numPoints,1, Scalar(255,255,255), 8 );

		//cout << maxCompactness << " | " << maxArea << endl;

		Point2f centroid = CalculateOpticalDiscCentroid(opticalContour);

		areaCompacness.push_back(maxCompactness);
		areaCompacness.push_back(maxArea);
		centroids.push_back(centroid);

		outputAreaVector.push_back(areaCompacness);

		morphologyEx(opticalMask, opticalMask, MORPH_DILATE, stucturingEllipseDilate, Point(-1,-1),2);

		debugDrawing = drawing;
		opticalDiskMask = opticalMask;

	} else {
		opticalDiskMask = Mat::zeros(image.rows, image.cols, CV_8UC1);
		debugDrawing = Mat::zeros(image.rows, image.cols, CV_8UC1);
	}

}

int Preprocessor::SortOpticalDiscData(vector<vector<double> >& areaInfo, vector<Point2f>& centroids) {

	float centroidMean_X = centroids[0].x;
	float centroidMean_Y = centroids[0].y;

	for(unsigned int i = 1 ; i < centroids.size() ; i++ ){
		centroidMean_X += centroids[i].x;
		centroidMean_Y += centroids[i].y;
	}
	centroidMean_X /= centroids.size();
	centroidMean_Y /= centroids.size();

	Point2f mean_center = Point2f(centroidMean_X , centroidMean_Y);


	vector<double> distances;
	vector<vector<double> > zipped;

	for(unsigned int i = 0 ; i < centroids.size() ; i++){
		double temp = norm(mean_center - centroids[i]);
		distances.push_back(temp);
	}


	for(unsigned int i = 0; i< distances.size(); i++) {
		vector<double> metrics;
		metrics.push_back(distances[i]);
		metrics.push_back(areaInfo[i][1]);
		metrics.push_back(areaInfo[i][0]);
		metrics.push_back(i);
		zipped.push_back(metrics);
	}


	sort(zipped.begin(), zipped.end() , Distance());


	//Take top 3

	/*
	double meanArea = 	zipped[0][1];
	for(int i = 1 ; i < 3; i ++){
		meanArea += zipped[i][1];
	}

	//Mean of the 3 area
	meanArea /= 3 ;
	//Min distance from mean
	double temp = 0;
	double minDis = abs(meanArea - zipped[0][1]);
	int resultIndex =0;
	for(int i = 1 ; i < 3 ; i++){
		temp = abs(meanArea - zipped[i][1]);
		if (minDis > temp){
			minDis = temp;
			resultIndex = zipped[i][2];
		}

	}
	*/
	return zipped[0][3];

}

void Preprocessor::ExtractMinEnclosingCircle (Mat& blobImage, Mat& mask) {

	Mat image;
	blobImage.copyTo(image);

	morphologyEx(image, image, MORPH_DILATE, getStructuringElement(1, Size(STRUCTURE_SIZE_1, STRUCTURE_SIZE_1)), Point(-1,-1),2);

	Mat maskImage = Mat::zeros(blobImage.size(), blobImage.type());

	vector<vector<Point> > contours;
	vector<Vec4i> hierarchy;

	findContours(image, contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE, Point(0,0)); //Find the contours of the thresholded objects (look like white blobs)

	vector<vector<Point> > contoursPoly (contours.size());
	vector<Point2f> center (contours.size());
	vector<float> radius (contours.size());

	for(unsigned int i = 0; i < contours.size(); i++) {
		approxPolyDP (Mat(contours[i]), contoursPoly[i], 3, true); //Find the approximate polygonal curve using the set of contour points found
		minEnclosingCircle((Mat)contoursPoly[i], center[i], radius[i]);
	}

	circle(maskImage,center[0],radius[0],Scalar(255,255,255),-1,8,0);

	mask = maskImage;


}


void Preprocessor::VessleSegmentation (Mat& image , Mat& mask){

	Mat stucturingEllipse = getStructuringElement(0, Size(STRUCTURE_SIZE_2, STRUCTURE_SIZE_2));
	Mat segImage;
	Mat segImage1;
	morphologyEx(image, segImage, MORPH_CLOSE, stucturingEllipse, Point(-1,-1),5);
	Mat bottomHat = abs(image - segImage);


	Mat test;
	bottomHat.copyTo(test);
	Size debugSize((int)segImage.cols/3, (int)segImage.rows/3);
	resize(test, test,debugSize, CV_INTER_CUBIC);
	imshow("test", test);

	threshold(bottomHat, bottomHat, 25,255, CV_THRESH_BINARY);

	morphologyEx(image, segImage1, MORPH_OPEN, stucturingEllipse, Point(-1,-1),3);
	Mat topHat = abs(image - segImage1);
	threshold(topHat, topHat, 25,255, CV_THRESH_BINARY);

	Mat output = abs(bottomHat - topHat);
	resize(output, output,debugSize, CV_INTER_CUBIC);
	resize(topHat, topHat, debugSize, CV_INTER_CUBIC);

	imshow("TopHat", topHat);
	imshow("outut", output);

}

Point2f Preprocessor::CalculateOpticalDiscCentroid(vector<Point> contours) {

	Point center;

	Moments opticalDiscMoment;
	opticalDiscMoment = moments(contours,false);

	center = Point2f(opticalDiscMoment.m10/opticalDiscMoment.m00,opticalDiscMoment.m01/opticalDiscMoment.m00);

	return center;


}

