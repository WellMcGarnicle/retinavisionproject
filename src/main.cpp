/*
 * main.cpp
 *
 *  Created on: May 10, 2015
 *      Author: nuttymtv1 and adam
 */

#include "BaseCV.h"
#include "main.h"
#include <sys/types.h>
#include <dirent.h>
#include <errno.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include "Preprocessor.h"
#include "KMeansSegmentation.cpp"
#include "SplitAndMerge.cpp"
#include "Analyse.cpp"
#include "Watershed.cpp"
#include "GraphCut.cpp"

const int K_MEANS = 0;
const int WATERSHED = 1;
const int SPLIT_AND_MERGE = 2;
const int GRAPH_CUT = 3;


struct Score {

	bool operator() (const vector<int> &scoreA , const vector<int> &scoreB){
		return scoreA[0] > scoreB[0];
	}
};

int main(int argc, char ** argv) {

     // read in image with command line argunment
	if(argc < 3 ){
		cout << "Usage ./project [-d|-i] [dir|file] " << endl;
		return -1;
	}

	vector<Mat> fundusImages;
	vector<string> fundusImageNames;

	vector<Mat> semiAutoImages;
	vector<Mat> semiMarkedImages;

	if((strcmp(argv[1], "-d") == 0)) {



		int loaded = LoadDirContents(argv[2], fundusImages, fundusImageNames);

		if(!loaded) {
			cout << "Error: Cannot open directory" << endl;
			return -1;
		}

		loaded = LoadSemiAutoContents(semiAutoImages,semiMarkedImages);

		if(!loaded) {
			cout << "Error: Cannot open directory" << endl;
			return -1;
		}

	} else if( (strcmp(argv[1], "-i") == 0)) {

		Mat image;
		int loaded = LoadImage(argv[2], image);

		if(!loaded) {
			cout << "Error: Can't read file" << endl;
			return -1;
		}

		fundusImages.push_back(image);
		fundusImageNames.push_back(argv[2]);


	} else {
		 cout << "Usage ./project [-d|-i] [dir|file] " << endl;
		 return -1;
	}

	vector<vector<Mat> > autoExudateMasks;
	vector<vector<Mat> > semiExudateMasks;

	ExtractExudates(fundusImages, semiAutoImages, semiMarkedImages, autoExudateMasks, semiExudateMasks);
	//Analyse analysis;
	//analysis.AnalyseImages(autoExudateMasks,semiExudateMasks,fundusImages,semiAutoImages);

	waitKey(0);

	return 0;
}


 bool LoadDirContents(string dirName, vector<Mat>& fundusImages, vector<string>& imageNames){

	 vector<string> fileNames;
	 vector<string> fundusFileNames;

	 string autoData[] = {"auto/image001.png","auto/image004.png","auto/image005.png",
	 				"auto/image006.png","auto/image007.png","auto/image012.png",
	 				"auto/image013.png","auto/image014.png","auto/image015.png",
	 				"auto/image016.png","auto/image017.png","auto/image019.png",
	 				"auto/image025.png","auto/image027.png","auto/image066.png",
	 				"auto/image068.png", "auto/image088.png"
	 };

	 for(unsigned int i = 0; i < 17; i++) {
		 //cout << fileNames[i] << endl;
		 //string fileName = dirName + "/" + fileNames[i];
		 string fileName = autoData[i];

		 Mat loadedImage;
		 bool loaded = LoadImage(fileName, loadedImage);

		 if(!loaded) {
			 cout << "Error: Can't read file " << fileName << endl;
		 } else {

			 fundusImages.push_back(loadedImage);
			 fundusFileNames.push_back(fileName);
		 }
	 }

	 if(fundusImages.size() == 0) {
		 cout << "No files found" << endl;
		 return false;
	 }

	 imageNames = fundusFileNames;

	 return true;

}

 bool LoadSemiAutoContents(vector<Mat>&  images, vector<Mat>& masks) {


	 string semiImages[] =  {"Semi/image005.png","Semi/image006.png", "Semi/image007.png", "Semi/image009.png","Semi/image012.png", "Semi/image013.png",
				"Semi/image014.png", "Semi/image015.png", "Semi/image016.png","Semi/image017.png","Semi/image019.png",
				"Semi/image025.png"
	};

	string semiMarked[] =  {"Semi/t005.png","Semi/t006.png", "Semi/t007.png", "Semi/t009.png","Semi/t012.png", "Semi/t013.png",
	 				"Semi/t014.png", "Semi/t015.png", "Semi/t016.png","Semi/t017.png","Semi/t019.png",
	 				"Semi/t025.png"
	};

	vector<Mat> real;
	vector<Mat> marked;

	for(int i = 0; i < 12; i++ ) {
		Mat loadedImageReal;
		loadedImageReal = imread(semiImages[i], CV_LOAD_IMAGE_COLOR);
		Mat loadedImageMarked;
		loadedImageMarked = imread(semiMarked[i], 0);


		if(!loadedImageReal.data || !loadedImageMarked.data) { //If the image can't be read
			return false;
		}

		marked.push_back(loadedImageMarked);
		real.push_back(loadedImageReal);

	}

	images = real;
	masks = marked;

	return true;

 }

 bool LoadImage(string imageName, Mat& image) {

 	Mat loadedImage;

 	loadedImage = imread(imageName, CV_LOAD_IMAGE_COLOR);

 	if(!loadedImage.data) { //If the image can't be read
 		return false;
 	}

 	loadedImage.copyTo(image);

 	return true; //Image successfully loaded

}

void ExtractExudates(vector<Mat>& fundusImages, vector<Mat>& semiAutoImages, vector<Mat>& markedImages, vector<vector<Mat> >& autoExudateImages,vector<vector<Mat> >& semiExudateImages ) {


	for(int method = 0; method < 3; method++) {

		vector<Mat> exudateMasks;
		vector<Mat> opticalDiscs;
		vector<Mat> editedImages;
		vector<vector<Mat> > processesImages;

		for (unsigned int i = 0; i < fundusImages.size(); i++) {

			Mat opticalDisc;
			Mat exudates;
			vector<Mat> processes;
			Mat empty;
			ExudateOperation(method, fundusImages[i], opticalDisc, exudates, processes,empty);

			Mat edit;
			OverlayExudateResult(exudates, fundusImages[i], true, edit);

			opticalDiscs.push_back(opticalDisc);
			exudateMasks.push_back(exudates);
			editedImages.push_back(edit);
			processesImages.push_back(processes);

			cout << "Image Complete: " << i << " of Method: " << method << endl;

		}

		autoExudateImages.push_back(exudateMasks);

		ParseResults(method, fundusImages, exudateMasks, opticalDiscs, editedImages, processesImages);


	}



	vector<Mat> exudateMasks;
	vector<Mat> opticalDiscs;
	vector<Mat> editedImages;
	vector<vector<Mat> > processesImages;

	for (unsigned int i = 0; i < semiAutoImages.size(); i++) {

		Mat opticalDisc;
		Mat exudates;
		vector<Mat> processes;
		Mat marking;
		markedImages[i].copyTo(marking);
		ExudateOperation(GRAPH_CUT, semiAutoImages[i], opticalDisc, exudates, processes,marking);

		Mat edit;
		OverlayExudateResult(exudates, semiAutoImages[i], true, edit);

		opticalDiscs.push_back(opticalDisc);
		exudateMasks.push_back(exudates);
		editedImages.push_back(edit);
		processesImages.push_back(processes);

		cout << i << endl;

	}

	semiExudateImages.push_back(exudateMasks);

	ParseResults(GRAPH_CUT, semiAutoImages, exudateMasks, opticalDiscs, editedImages, processesImages);




}

void ParseResults(int method, vector<Mat>& images, vector<Mat>& exudates, vector<Mat>&opticalDiscs, vector<Mat>& edits, vector<vector<Mat> >&processes) {

	string methodsDirectory[] = {"Results/KMeans", "Results/Watershed" , "Results/SplitNMerge", "Results/Graphcut"};
	Analyse analysis;
	vector<vector<int> > scoreResults;

	for(unsigned int i = 0 ; i < exudates.size(); i++) {

		int score = analysis.CountExudatePixels(exudates[i]);
		vector<int> scoreVect;
		scoreVect.push_back(score);
		scoreVect.push_back(i);
		scoreResults.push_back(scoreVect);

	}

	sort(scoreResults.begin(), scoreResults.end() , Score());

	ostringstream ss;
	ss << "mkdir \"";
	ss << methodsDirectory[method];
	ss << "\"";
	string command(ss.str());

	ostringstream ss_1;
	ss_1 << methodsDirectory[method];
	ss_1 << "/result.txt";
	string resultFile(ss_1.str());

	system(command.c_str());

	cout << resultFile << endl;
	ofstream file;;
	file.open(resultFile.c_str(), ofstream::out | ofstream::trunc);
	if(file.is_open())
	{
		for(unsigned int i = 0 ; i < scoreResults.size(); i++) {
			int score = scoreResults[i][0];
			int grade = 1;
			if(score > 50 && score < 1100) {
				grade = 2;
			} else if (score >= 1000) {
				grade = 3;
			}

			ostringstream ss;
			ss << "Image" ;
			ss << scoreResults[i][1];
			ss << " | ";
			ss << grade;
			ss << " | ";
			ss << score;
			ss << "\n";

			string line(ss.str());

			cout << line << endl;

			file << line;

		}

		file.close();
	}


	for(unsigned int i = 0 ; i < scoreResults.size(); i++) {

		ss.str("");
		ss.clear();
		ss << "mkdir \"";
		ss << methodsDirectory[method];
		ss << "/Image" ;
		ss << scoreResults[i][1];
		ss << "\"";
		string commandImage(ss.str());

		ss.str("");
		ss.clear();
		ss << methodsDirectory[method];
		ss << "/Image" ;
		ss << scoreResults[i][1];
		ss << "/original.jpg";
		string originalName(ss.str());

		ss.str("");
		ss.clear();
		ss << methodsDirectory[method];
		ss << "/Image" ;
		ss << scoreResults[i][1];
		ss << "/marked.jpg";
		string markedName(ss.str());

		system(commandImage.c_str());

		Mat markedConv;
		edits[scoreResults[i][1]].copyTo(markedConv);
		vector<int> qualityType;
		qualityType.push_back(CV_IMWRITE_JPEG_QUALITY);
		qualityType.push_back(90);

		imwrite(originalName, images[scoreResults[i][1]]);
		imwrite(markedName, markedConv, qualityType);

		unsigned int j = 0;
		for(j = 0; j < processes[scoreResults[i][1]].size() ; j++) {

			ss.str("");
			ss.clear();
			ss << methodsDirectory[method];
			ss << "/Image" ;
			ss << scoreResults[i][1];
			ss << "/process";
			ss << j;
			ss << ".jpg";
			string processName(ss.str());

			Mat procConv;
			processes[scoreResults[i][1]][j].copyTo(procConv);
			imwrite(processName, procConv, qualityType);

		}
		j++;
		ss.str("");
		ss.clear();
		ss << methodsDirectory[method];
		ss << "/Image" ;
		ss << scoreResults[i][1];
		ss << "/process";
		ss << j;
		ss << ".jpg";
		string opticalName(ss.str());

		Mat optical;
		opticalDiscs[scoreResults[i][1]].copyTo(optical);
		imwrite(opticalName, optical, qualityType);

		j++;
		ss.str("");
		ss.clear();
		ss << methodsDirectory[method];
		ss << "/Image" ;
		ss << scoreResults[i][1];
		ss << "/process";
		ss << j;
		ss << ".jpg";
		string exudateName(ss.str());

		Mat exudatesConv;
		exudates[scoreResults[i][1]].copyTo(exudatesConv);
		imwrite(exudateName, exudatesConv, qualityType);

	}


}

void ExudateOperation(int method, Mat& image, Mat& segmentedOpticalDisc, Mat& segmentedExudates, vector<Mat>& processImages, Mat& markedImage) {

	Preprocessor preprocess(0);

	Mat originalImage;
	image.copyTo(originalImage);
	preprocess.ImageCorrection(originalImage);

	Mat greenChannelContrast;
	originalImage.copyTo(greenChannelContrast);
	Size newSize((int)greenChannelContrast.cols/3, (int)greenChannelContrast.rows/3);
	resize(greenChannelContrast, greenChannelContrast,newSize, CV_INTER_CUBIC);
	processImages.push_back(greenChannelContrast);

	Mat opticalDiscMask;
	Point2f opticalDiscCenter;
	preprocess.OpticalDiscSegmentation(originalImage, opticalDiscMask, opticalDiscCenter);

	Mat opticalCircle;
	preprocess.ExtractMinEnclosingCircle(opticalDiscMask,opticalCircle);

	Mat exudateMask;

	if(method == K_MEANS) {
		KMeansSegmentation segMethod;
		segMethod.ExtractExudates(originalImage, exudateMask, opticalDiscCenter, opticalDiscMask, processImages);
	} else if (method == WATERSHED) {
		Watershed segMethod;
		segMethod.ExtractExudates(originalImage,image, opticalCircle,opticalDiscCenter, exudateMask, processImages);
	} else if (method == SPLIT_AND_MERGE) {
		SplitAndMerge segMethod;
		segMethod.ExtractExudates(originalImage,exudateMask,opticalDiscCenter, opticalCircle, processImages);
	} else if (method == GRAPH_CUT) {
		GraphCut segMethod;
		segMethod.ExtractExudates(image, markedImage, opticalCircle,opticalDiscCenter,exudateMask, processImages);
	}

	segmentedOpticalDisc = opticalCircle;
	segmentedExudates = exudateMask;

}


 void DebugImageShow(Mat& image) {

	 Mat debugImage;
	 image.copyTo(debugImage);
	 Size debugSize((int)debugImage.cols/3, (int)debugImage.rows/3);
	 resize(debugImage, debugImage,debugSize, CV_INTER_CUBIC);
	 imshow("Debug Show", debugImage);


 }

 void OverlayExudateResult(Mat& exudateMask, Mat& originalImage, bool resizeImage, Mat& edit) {

	 Mat image;
	 originalImage.copyTo(image);
	 if(resizeImage) {
		 Size debugSize((int)image.cols/3, (int)image.rows/3);
		 resize(image, image,debugSize, CV_INTER_CUBIC);
	 }

	 Mat maskOverlay(exudateMask.size(), CV_8UC4,Scalar(0,0,0,0));
	 maskOverlay.setTo(Scalar(0,255,0),exudateMask);

	 Mat alphaImage = Mat(image.size(),CV_8UC4);

	 int from_to[] = {0,0,1,1,2,2,3,3};

	 mixChannels(&image, 2, &alphaImage,1, from_to,4);


	 Mat editedImage;
	 addWeighted(alphaImage, 1, maskOverlay, 0.5,0.0, editedImage);

	 edit = editedImage;

 }




