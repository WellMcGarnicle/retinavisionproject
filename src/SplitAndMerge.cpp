
#include "BaseCV.h"
#include <iostream>
#include <stdio.h>
#include <string>
#include <stdlib.h>

using namespace cv;
using namespace std;

struct Quad
{

	vector<Quad> children;
	bool isLeaf;

	Rect roi;
	Mat roiImage;
	Scalar quadInformation;

};

class SplitAndMerge
{
public:

	void ExtractExudates(Mat& image, Mat& finalExudateMask, Point2f opticalCenter, Mat& opticalMask, vector<Mat>& processes){


		Mat processedImage;
		SplitAndMergePreprocessing(image,processedImage);

		Mat operatedImage;
		MaskOpticalDisc(processedImage,opticalMask,operatedImage);

		Mat exudateMask;
		SplitAndMergeProcess(operatedImage, image, opticalMask, exudateMask, processes);

		finalExudateMask = exudateMask;


	}

private:

	void SplitAndMergePreprocessing(Mat& image, Mat& processedImage) {

		Mat morphImage;
		image.copyTo(morphImage);

		Size convertSize((int)morphImage.cols/3, (int)morphImage.rows/3);
		resize(morphImage, morphImage,convertSize, CV_INTER_CUBIC);

		//imshow("Input", morphImage);

		Mat stucturingEllipse = getStructuringElement(1, Size(8, 8));

		morphologyEx(morphImage, morphImage, MORPH_CLOSE, stucturingEllipse, Point(-1,-1),1);



		processedImage = morphImage;



	}

	void MaskOpticalDisc(Mat& image, Mat& opticalMask, Mat& resultImage) {

		Mat avgVal;
		image.copyTo(avgVal);
		Scalar avgIntensity = mean(avgVal);

		Mat maskOverlay(opticalMask.size(), CV_8UC1,Scalar(0,0,0,0));
		maskOverlay.setTo(Scalar(avgIntensity[0],avgIntensity[0],avgIntensity[0]),opticalMask);

		Mat editedImage;
		subtract(image, opticalMask, editedImage);
		add(editedImage, maskOverlay, editedImage);

		resultImage = editedImage;

	}

	void SplitAndMergeProcess(Mat& inputImage, Mat& firstImage, Mat& opticalDisc, Mat& finalExudateMask, vector<Mat>& processes) {

		Mat operatedImage;
		inputImage.copyTo(operatedImage);

		GaussianBlur(operatedImage,operatedImage, Size(3,3),0,0);
		processes.push_back(operatedImage);

		Mat originalImage;
		Mat squareImage;
		operatedImage.copyTo(squareImage);
		inputImage.copyTo(originalImage);

		int exponent = log(min(squareImage.cols,  squareImage.rows)) / log(2);
		int s = pow(2.0, (double) exponent);

		int sq2Cut = abs(squareImage.cols-s);
		int sq3Cut = abs(squareImage.rows-s);

		Rect square1 = Rect(0,0,s,s);
		Rect square2 = Rect(sq2Cut,0,s,s);
		Rect square3 = Rect(0,sq3Cut,s,s);
		Rect square4 = Rect(sq2Cut,sq3Cut,s,s);

		Mat splitImage1 = squareImage(square1).clone();
		Mat splitImage2 = squareImage(square2).clone();
		Mat splitImage3 = squareImage(square3).clone();
		Mat splitImage4 = squareImage(square4).clone();

		Mat originImage1 = originalImage(square1).clone();
		Mat originImage2 = originalImage(square2).clone();
		Mat originImage3 = originalImage(square3).clone();
		Mat originImage4 = originalImage(square4).clone();

		Quad q1 = QuadSplit(splitImage1,square1);
		Quad q2 = QuadSplit(splitImage2,square1);
		Quad q3 = QuadSplit(splitImage3,square1);
		Quad q4 = QuadSplit(splitImage4,square1);

		Mat split1Output;
		splitImage1.copyTo(split1Output);
		DrawRectangleQuads(split1Output,q1);
		processes.push_back(split1Output);

		Mat split2Output;
		splitImage2.copyTo(split2Output);
		DrawRectangleQuads(split2Output,q2);
		processes.push_back(split2Output);

		Mat split3Output;
		splitImage3.copyTo(split3Output);
		DrawRectangleQuads(split3Output,q3);
		processes.push_back(split3Output);

		Mat split4Output;
		splitImage4.copyTo(split4Output);
		DrawRectangleQuads(split4Output,q4);
		processes.push_back(split4Output);

		QuadMerge(splitImage1,q1);
		Mat merge1Output;
		splitImage1.copyTo(merge1Output);
		DrawRectangleQuads(merge1Output,q1);
		processes.push_back(merge1Output);

		QuadMerge(splitImage2,q2);
		Mat merge2Output;
		splitImage2.copyTo(merge2Output);
		DrawRectangleQuads(merge2Output,q2);
		processes.push_back(merge2Output);

		QuadMerge(splitImage3,q3);
		Mat merge3Output;
		splitImage3.copyTo(merge3Output);
		DrawRectangleQuads(merge3Output,q3);
		processes.push_back(merge3Output);

		QuadMerge(splitImage4,q4);
		Mat merge4Output;
		splitImage4.copyTo(merge4Output);
		DrawRectangleQuads(merge4Output,q4);
		processes.push_back(merge4Output);

		int colorCount1[256] = {0};
		Mat hist1Output;
		originImage1.copyTo(hist1Output);
		BinCountQuads(hist1Output, q1, colorCount1);

		int colorCount2[256] = {0};
		Mat hist2Output;
		originImage2.copyTo(hist2Output);
		BinCountQuads(hist2Output, q2, colorCount2);

		int colorCount3[256] = {0};
		Mat hist3Output;
		originImage3.copyTo(hist3Output);
		BinCountQuads(hist3Output, q3, colorCount3);

		int colorCount4[256] = {0};
		Mat hist4Output;
		originImage4.copyTo(hist4Output);
		BinCountQuads(hist4Output, q4, colorCount4);

		vector<int> threshes;
		threshes.push_back(HighestSubsetThreshold(colorCount1));
		threshes.push_back(HighestSubsetThreshold(colorCount2));
		threshes.push_back(HighestSubsetThreshold(colorCount3));
		threshes.push_back(HighestSubsetThreshold(colorCount4));
		int maxThresh = 0;
		for(unsigned int i = 0 ; i < threshes.size(); i++) {
			if(threshes[i] > maxThresh) {
				maxThresh = threshes[i];
			}
		}

		Mat threshPre;
		firstImage.copyTo(threshPre);
		threshold(firstImage,threshPre,maxThresh,255,THRESH_BINARY);
		Size convertSize((int)threshPre.cols/3, (int)threshPre.rows/3);
		resize(threshPre, threshPre,convertSize, CV_INTER_CUBIC);
		processes.push_back(threshPre);

		Mat subtractingMask;
		Mat finalExudates;
		ExudateFiltering(threshPre, subtractingMask, finalExudates, opticalDisc);
		processes.push_back(subtractingMask);

		finalExudateMask = finalExudates;
	}

	Quad QuadSplit(Mat& quadImage,Rect roi) {

		Quad q;
		vector<Quad> children;

		q.roi = roi;
		q.roiImage = quadImage;
		q.isLeaf = true;
		q.children = children;

		bool toSplit = SignificantStddev(quadImage,5.0);

		if(toSplit) {

			int newWidth = quadImage.cols/2;
			int newHeight = quadImage.rows/2;

			Rect split1 = Rect(0,0,newWidth, newHeight);
			Rect split1ROI = Rect(roi.x,roi.y, newWidth, newHeight);
			Rect split2 = Rect(newWidth,0,newWidth, newHeight);
			Rect split2ROI = Rect(roi.x+newWidth,roi.y, newWidth, newHeight);
			Rect split3 = Rect(0,newHeight,newWidth, newHeight);
			Rect split3ROI = Rect(roi.x,roi.y+newHeight, newWidth, newHeight);
			Rect split4 = Rect(newWidth,newHeight,newWidth, newHeight);
			Rect split4ROI = Rect(roi.x+newWidth,roi.y+newHeight, newWidth, newHeight);

			Mat splitImage1 = quadImage(split1).clone();
			Mat splitImage2 = quadImage(split2).clone();
			Mat splitImage3 = quadImage(split3).clone();
			Mat splitImage4 = quadImage(split4).clone();

			Quad quad1 = QuadSplit(splitImage1, split1ROI);
			Quad quad2 = QuadSplit(splitImage2, split2ROI);
			Quad quad3 = QuadSplit(splitImage3, split3ROI);
			Quad quad4 = QuadSplit(splitImage4, split4ROI);

			q.children.push_back(quad1);
			q.children.push_back(quad2);
			q.children.push_back(quad3);
			q.children.push_back(quad4);


		} else {

			Scalar stddev;
			Scalar mean;
			meanStdDev(quadImage,mean, stddev);
			q.quadInformation = mean;

		}

		return q;

	}

	void QuadMerge(Mat& quadImage,Quad& q) {


		if(q.children.size() == 0) {
			return;
		}

		bool isMerged1 = false;
		bool isMerged2 = false;

		isMerged1 = MergeGivenQuads(quadImage, q.children[0], q.children[1]);
		isMerged2 = MergeGivenQuads(quadImage, q.children[2], q.children[3]);

		if(!isMerged1 && !isMerged2) {

			isMerged1 = MergeGivenQuads(quadImage, q.children[0], q.children[2]);
			isMerged2 = MergeGivenQuads(quadImage, q.children[1], q.children[3]);

		}

		for(unsigned int i = 0 ; i < q.children.size() ; i++) {
			if(q.children[i].children.size() > 0) {
				QuadMerge(quadImage, q.children[i]);
			}
		}

	}

	bool MergeGivenQuads(Mat& inputImage, Quad& q1, Quad& q2) {

		if(q1.children.size() == 0 && q2.children.size()== 0) {

			Rect roi1 = q1.roi;
			Rect roi2 = q2.roi;
			Rect mergedROI = MergeGivenROI(roi1, roi2);

			Mat mergedQuad = inputImage(mergedROI).clone();

			bool significant = SignificantStddev(mergedQuad,3.0);

			if(!significant) {

				q1.roi = mergedROI;
				q2.isLeaf = false;
				return true;

			}
		}

		return false;

	}


	Rect MergeGivenROI(Rect r1, Rect r2) {

		int left = min(r1.x, r2.x);
		int top = min(r1.y, r2.y);
		int right = max(r1.x + r1.width, r2.x + r2.width);
		int bottom = max(r1.y + r1.height, r2.y + r2.height);

		Rect merged = Rect(left,top,right-left, bottom-top);

		return merged;

	}



	void DrawRectangleQuads(Mat& image, Quad q) {

		if(q.isLeaf && q.children.size() == 0) {
			rectangle(image, q.roi,50,0.1);
		} else {

			for(unsigned int i = 0 ; i < q.children.size(); i++) {
				DrawRectangleQuads(image, q.children[i]);
			}

		}

	}

	void BinCountQuads(Mat& image, Quad q, int (&colorCount)[256]) {

		if(q.isLeaf && q.children.size() == 0) {
			Mat quad = image(q.roi).clone();
			Scalar stddev;
			Scalar mean;
			meanStdDev(quad,mean, stddev);
			colorCount[(int)mean[0]]++;

		} else {

			for(unsigned int i = 0 ; i < q.children.size(); i++) {
				BinCountQuads(image, q.children[i], colorCount);
			}

		}


	}

	bool SignificantStddev(Mat& inputImage, double significance) {

		Scalar stddev;
		Scalar mean;

		meanStdDev(inputImage,mean, stddev);

		if(stddev[0] >= significance && (inputImage.cols*inputImage.rows > 16)) {
			return true;
		}

		return false;

	}

	int HighestSubsetThreshold(int (&hist)[256]) {

		int threshold = 255;

		int count = 40;

		while(count > 0) {

			if(hist[threshold] != 0) {
				count--;
			}

			threshold--;

		}


		return threshold;


	}

	void ExudateFiltering(Mat& initialExudates, Mat& subtractMask, Mat& result, Mat& opticalDisc) {

		Mat image;
		initialExudates.copyTo(image);

		Mat stucturingEllipse = getStructuringElement(1, Size(5,5));
		morphologyEx(image, image, MORPH_CLOSE, stucturingEllipse, Point(-1,-1),1);

		vector<vector<Point> > contours;
		vector<Vec4i> hierarchy;

		findContours(image, contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE, Point(0,0)); //Find the contours of the thresholded objects (look like white blobs)

		vector<vector<Point> > contoursPoly (contours.size());
		vector<Point2f> center (contours.size());
		vector<float> radius (contours.size());

		for(unsigned int i = 0; i < contours.size(); i++) {
			approxPolyDP (Mat(contours[i]), contoursPoly[i], 3, true); //Find the approximate polygonal curve using the set of contour points found

		}

		Mat maskBase = Mat::zeros(image.rows, image.cols, CV_8UC1);

		for(unsigned int i = 0; i < contoursPoly.size(); i++) {

			double contArea = contourArea(contoursPoly[i], false);

			if(contArea >= 1600) {
				vector<Point> maskContour = contoursPoly[i];
				const Point* contourPoints[1] = { &maskContour[0] };
				int numPoints = (int)maskContour.size();
				fillPoly(maskBase,contourPoints,&numPoints,1, Scalar(255,255,255), 8 );
			}

		}

		add(maskBase, opticalDisc, maskBase);
		morphologyEx(maskBase, maskBase, MORPH_DILATE, stucturingEllipse, Point(-1,-1),1);

		subtract(initialExudates, maskBase, result);
		subtractMask = maskBase;

	}


};
