
#include "BaseCV.h"
#include <iostream>
#include <stdio.h>
#include <string>

using namespace cv;
using namespace std;

class KMeansSegmentation
{

public:

	void ExtractExudates(Mat image, Mat& finalExudateMask, Point2f opticalCenter, Mat& opticalMask, vector<Mat>& processes) {

		Mat inputImage;
		image.copyTo(inputImage);
		cvtColor(inputImage, inputImage, CV_GRAY2RGB);
		Mat kMeansMatrix = Mat::zeros(inputImage.cols*inputImage.rows,5, CV_32F);
		Mat bestLabels;
		Mat clusterCenters;

		MatrixConvert(inputImage, kMeansMatrix);

		int kClusters = 5;
		kmeans(kMeansMatrix,kClusters,bestLabels, TermCriteria(CV_TERMCRIT_EPS+CV_TERMCRIT_ITER,10,1.0),3,KMEANS_PP_CENTERS, clusterCenters);

		int clusterColors[kClusters];
		for(int i = 0; i < kClusters; i++) {
			clusterColors[i] = 255 /(i+1);
		}

		Mat kMeansOutput(inputImage.rows,inputImage.cols, CV_32F);
		for(int i = 0; i < inputImage.cols * inputImage.rows; i++) {
				kMeansOutput.at<float>(i/inputImage.cols,i%inputImage.cols) = (float)(clusterColors[bestLabels.at<int>(0,i)]);
		}

		kMeansOutput.convertTo(kMeansOutput, CV_8U);
		Size debugSize((int)kMeansOutput.cols/3, (int)kMeansOutput.rows/3);
		resize(kMeansOutput, kMeansOutput,debugSize, CV_INTER_CUBIC);
		processes.push_back(kMeansOutput);
		//imshow("Kmeans", kMeansOutput);

		Mat exudateRegion;
		ExtractKMeansExudates(kMeansOutput, exudateRegion,opticalCenter, opticalMask);
		//imshow("Exudate Region", exudateRegion);

		Mat edges;
		KirschOperator(image, edges);
		//imshow("Kirsh Output", edges);
		processes.push_back(edges);

		Mat threshKirsh;
		threshold(edges, threshKirsh,180, 255, THRESH_BINARY);
		//morphologyEx(threshKirsh, threshKirsh, MORPH_DILATE, getStructuringElement(0, Size(3, 3)), Point(-1,-1),1);
		//imshow("Kirsh Thresh", threshKirsh);


		Mat andOutput;
		bitwise_and(exudateRegion,threshKirsh, andOutput);
		//morphologyEx(andOutput, andOutput, MORPH_DILATE, getStructuringElement(0, Size(3, 3)), Point(-1,-1),0);
		//imshow("Exudates AND", andOutput);

		subtract(andOutput, opticalMask, andOutput);

		finalExudateMask = andOutput;




	}

private:

	void MatrixConvert(Mat& inputImage, Mat& kMeansMatrix) {

		vector<Mat> RGB;
		split(inputImage, RGB);
		for(int i = 0; i < inputImage.cols*inputImage.rows; i++) {
			kMeansMatrix.at<float>(i,0) = (i/inputImage.cols)/inputImage.rows;
			kMeansMatrix.at<float>(i,1) = (i%inputImage.cols)/inputImage.cols;
			kMeansMatrix.at<float>(i,2) = RGB[0].data[i] / 255.0;
			kMeansMatrix.at<float>(i,3) = RGB[1].data[i] / 255.0;
			kMeansMatrix.at<float>(i,4) = RGB[2].data[i] / 255.0;
		}

	}

	int DominantColor(Mat& image) {
		int histSize = 256;
		float range[] = {0,256};
		const float* histRange = {range};
		bool uniform = true;
		bool accumulate = false;

		Mat hist;
		calcHist(&image,1,0,Mat(),hist,1,&histSize,&histRange, uniform, accumulate);

		for(int i = 0 ; i < hist.cols ; i++) {
			for(int j = 0; j < hist.rows; j++) {
				Vec3b pixelInformation = hist.at<Vec3b>(i,j);
				int freq = pixelInformation.val[0];

				if(freq != 0 ) {
					cout << j << ":" << freq << " ";
				}
			}

			cout << endl;
		}

		return 0;
	}

	void ExtractKMeansExudates(Mat& image, Mat& exudateOutput, Point2f opticalDiscPoint, Mat& opticalDiscMask) {

		Rect roi = Rect(Point(opticalDiscPoint.x+15,opticalDiscPoint.y+15), Point(opticalDiscPoint.x-15,opticalDiscPoint.y-15));

		Mat temp;
		image.copyTo(temp);
		circle(temp,opticalDiscPoint,3,Scalar(10,10,10),-1,8,0);
		rectangle(temp,roi,1,8,0);
		//imshow("Translate", temp);

		Mat opticalAndImage;
		bitwise_and(image,opticalDiscMask,opticalAndImage);
		//imshow("Optical AND", opticalAndImage);

		Mat region = image(roi);
		//imshow("Region", region);

		//Vec3b pixelInformation = image.at<Vec3b>(150.0,100.0)[0];
		//int thresholdVal = pixelInformation.val[0];
		int thresholdVal;

		int clusterBucket[] = {255,127,85,63,51};
		int pixelBuckets[] = {0,0,0,0,0};

		for(float i = 0 ; i < opticalAndImage.cols ; i++) {
			for(float j = 0; j < opticalAndImage.rows; j++) {
				//cout << i << ", " << j << endl;
				Vec3b pixelInf = opticalAndImage.at<Vec3b>(i,j)[0];
				int thresholdV = pixelInf.val[0];
				//cout << thresholdV << endl;

				if (thresholdV == 255) {
					pixelBuckets[0]++;
				} else if (thresholdV == 127)
					pixelBuckets[1]++;
				else if(thresholdV == 85) {
					pixelBuckets[2]++;
				} else if (thresholdV == 63) {
					pixelBuckets[3]++;
				} else if (thresholdV == 51) {
					pixelBuckets[4]++;
				}

			}
		}
		int maxIndex = 0;
		int maxVal = 0;
		for(int i = 0; i < 5 ; i++) {
			if(pixelBuckets[i] > maxVal) {
				maxVal = pixelBuckets[i];
				maxIndex = i;
			}
		}

		thresholdVal = clusterBucket[maxIndex];

		cout << "Thresh Index = " << maxIndex << endl;
		cout << opticalDiscPoint << "|" << thresholdVal << endl;



		Mat exudateRegion;
		inRange(image,thresholdVal-10, thresholdVal+10, exudateRegion);
		exudateOutput = exudateRegion;

	}

	void KirschOperator(Mat& image, Mat& output) {

		int kernalSize = 3;

		Mat operatedImage;
		image.copyTo(operatedImage);
		Size convertSize((int)operatedImage.cols/3, (int)operatedImage.rows/3);
		resize(operatedImage, operatedImage,convertSize, CV_INTER_CUBIC);

		Mat kirschKernal = Mat(kernalSize,kernalSize, CV_32F);

		kirschKernal.at<float>(0,0) = 5;
		kirschKernal.at<float>(0,1) = -3;
		kirschKernal.at<float>(0,2) = -3;
		kirschKernal.at<float>(1,0) = 5;
		kirschKernal.at<float>(1,1) = 0;
		kirschKernal.at<float>(1,2) = -3;
		kirschKernal.at<float>(0,0) = 5;
		kirschKernal.at<float>(0,1) = -3;
		kirschKernal.at<float>(0,2) = -3;

		Mat kirschKernalResult;
		filter2D(operatedImage, kirschKernalResult,-1,kirschKernal);
		output = kirschKernalResult;

	}

};
