/*
 * Perceptron.cpp
 *
 *  Created on: 27/05/2015
 *      Author: adam
 */

#include <iostream>
#include <stdio.h>
#include <string>
#include <time.h>
#include <stdlib.h>
#include <vector>
#include <math.h>


using namespace std;

class Perceptron
{

public:


	Perceptron(int numOfInputs) {

		for(int i =0; i < numOfInputs; i++) {
			float random = ((float) rand()) / (float)RAND_MAX;
			float diff = -1.0;
			float r = random * diff;
			r = 0.5+r;
			cout << r << endl;
			r=0;
			weights.push_back(r);
		}

	}

	float ForwardPass(vector<float> inputVector) {

		float result = 0.0;

		for(int i = 0; i < inputVector.size(); i++) {
			float weight = weights[i];
			float input =inputVector[i];
			result += weight * input;
		}

		/*
		if(result >= 0 ) {
			return 1.0;
		} else {
			return 0.0;
		}
		*/

		//cout << result << endl;

		result = 1.0/(1.0 + exp(-result));
		return result;

	}


	void AdjustPerceptronWeights(float learningRate, float target, float output, vector<float> input) {

		for(int i = 0 ; i < weights.size(); i++) {

			float newWeight =  weights[i] + learningRate*(target-output)*input[i];

			weights[i] = newWeight;

		}

	}

private:

	vector<float> weights;




};
