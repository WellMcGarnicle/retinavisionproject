/*
 * GraphCut.cpp
 *
 *  Created on: 29/05/2015
 *      Author: adam
 */
#include "BaseCV.h"
#include <iostream>
#include <stdio.h>
#include <string>

using namespace cv;
using namespace std;

class GraphCut {

public:

	void ExtractExudates(Mat& originalImage, Mat& markedMask, Mat& opticalMask,  Point2f opticalDiscPoint, Mat& finalResult, vector<Mat>& processImages) {

		Mat original;
		Mat marked;

		originalImage.copyTo(original);
		markedMask.copyTo(marked);

		resize(original, original,Size((int)original.cols/3, (int)original.rows/3), CV_INTER_CUBIC);
		resize(marked, marked,Size((int)marked.cols/3, (int)marked.rows/3), CV_INTER_CUBIC);

		processImages.push_back(marked);

		//imshow("Graph Cut Mask," , original);
		//imshow("Graph Cut Image", marked);


		Mat grabCutMask = Mat(marked.size(), marked.type());

		CreateGrabCutMask(marked, grabCutMask);
		Mat bdgModel; Mat fgdModel;


		grabCut(original,grabCutMask, Rect(),bdgModel, fgdModel,5,GC_INIT_WITH_MASK);

		Mat probableExudate;
		Mat definiteExudate;
		Mat finalMask;
		compare(grabCutMask, GC_PR_FGD, probableExudate,CMP_EQ);
		compare(grabCutMask, GC_FGD, definiteExudate,CMP_EQ);
		add(probableExudate,definiteExudate, finalMask);

		Mat whiteForeground(original.size(),CV_8UC1,Scalar(255,255,255));
		Mat exudatesMask(original.size(),CV_8UC1,Scalar(0,0,0));
		whiteForeground.copyTo(exudatesMask, finalMask);
		subtract(exudatesMask, opticalMask, exudatesMask);

		Mat foreground(original.size(),CV_8UC3,Scalar(255,255,255));
		original.copyTo(foreground, finalMask);

		processImages.push_back(foreground);

		//imshow("Result Graph", foreground);
		//imshow("Exudates Mask", exudatesMask);

		finalResult = exudatesMask;


	}

private:

	void CreateGrabCutMask(Mat& markedMask, Mat& resultMask) {

		//Mat newMask = Mat(markedMask.size(), markedMask.type());

		Mat newMask;
		markedMask.copyTo(newMask);

		for(int i = 0 ; i < markedMask.cols ; i++) {
			for(int j = 0; j <markedMask.rows; j++) {

				int  intensity = markedMask.at<uchar>(j,i); //white

				//cout << intensity << endl;


				if(intensity == 255) {
					//markedMask.at<uchar>(j,i) = 0;
					newMask.at<uchar>(j,i) = GC_FGD;
				} else if (intensity == 0) {
					newMask.at<uchar>(j,i) = GC_BGD;
				} else {
					newMask.at<uchar>(j,i) = GC_PR_BGD;
				}

			}
		}


		resultMask = newMask;


	}

};



