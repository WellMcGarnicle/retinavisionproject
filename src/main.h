/*
 * main.h
 *
 *  Created on: May 10, 2015
 *      Author: nuttymtv1
 */

#ifndef MAIN_H_
#define MAIN_H_

#include <iostream>
#include <vector>
#include <stdio.h>

using namespace cv;
using namespace std;

bool LoadDirContents(string dirName, vector<Mat>& fundusImages);
bool LoadImage(string imageName, Mat& image);
bool LoadDirContents(string dirName, vector<Mat>& fundusImages, vector<string>& imageNames);
void OverlayExudateResult(Mat& exudateMask, Mat& originalImage, bool resizeImage, Mat& edit);
bool LoadSemiAutoContents(vector<Mat>&  images, vector<Mat>& masks);
void ExtractExudates(vector<Mat>& fundusImages, vector<Mat>& semiAutoImages, vector<Mat>& markedImages,vector<vector<Mat> >& autoExudateImages,vector<vector<Mat> >& semiExudateImages);
void ExudateOperation(int method, Mat& image, Mat& segmentedOpticalDisc, Mat& segmentedExudates, vector<Mat>& processImages, Mat& markedImage);
void ParseResults(int method, vector<Mat>& images, vector<Mat>& exudates, vector<Mat>&opticalDiscs, vector<Mat>& edits, vector<vector<Mat> >&processes);

void DebugImageShow(Mat& image);

#endif /* MAIN_H_ */
