/*
 * analyse.cpp
 *
 *  Created on: 26/05/2015
 *      Author: adam
 */

#include "BaseCV.h"
#include <iostream>
#include <stdio.h>
#include <string>
#include "Perceptron.cpp"

using namespace cv;
using namespace std;

class Analyse
{

public:


	void AnalyseImages(vector<vector<Mat> >& autoExudates, vector<vector<Mat> >& semiExudates, vector<Mat>& autoImages, vector<Mat>& semiImages) {

		vector<Mat> autoGroundData; vector<Mat> semiGroundData;

		bool loaded = LoadGroundData(autoGroundData,semiGroundData);

		if(!loaded) {
			cout << "Can't load ground data";
		}

		for(int imageNum = 0; imageNum < 17; imageNum++) {

			Mat groundTruth;
			ExtractMarkedMask(autoGroundData[imageNum],groundTruth );


			Mat originalImage;
			autoImages[imageNum].copyTo(originalImage);
			Size newSize((int)originalImage.cols/3, (int)originalImage.rows/3);
			resize(originalImage, originalImage,newSize, CV_INTER_CUBIC);

			Mat exudateMask;
			autoExudates[0][imageNum].copyTo(exudateMask);

			cout  << "-----------------" << "Image: " << imageNum << "-------------------" << endl;

			int TP = CalculateTruePositive(groundTruth,exudateMask);
			cout << "True Positive: " << TP << endl;

			int FP = CalculateFalsePositive(groundTruth,exudateMask);
			cout << "False Positive: " << FP << endl;

			int TN = CalculateTrueNegative(groundTruth,exudateMask);
			cout << "True Negative: " << TN << endl;

			int FN = CalculateFalseNegative(groundTruth,exudateMask);
			cout << "False Negative: " << FN << endl;


			double sensitivity = (double)TP / ((double)TP + (double)FN);
			double PPV = (double)TP / ((double)TP+(double)FP);
			double accuracy = ((double)TP + (double)TN) / ((double)TP+(double)FP+(double)TN+(double)FN);

			cout << "Sensitivity: " << sensitivity*100.0<< endl;
			cout << "PPV: " << PPV*100.0 << endl;
			cout << "Accuracy: " << accuracy*100.0 << endl;
		}




	}



	int CountExudatePixels(Mat& exudateMask) {

		int count = 0;

		for(int i = 0 ; i < exudateMask.cols ; i++) {
			for(int j = 0; j <exudateMask.rows; j++) {

				int  intensity = exudateMask.at<uchar>(j,i); //white

				if(intensity == 255) {
					count++;
				}

			}
		}

		return count;

	}


private:


	int CalculateTruePositive(Mat& groundTruth, Mat& exudateMask) {

		Mat result;
		bitwise_and(groundTruth, exudateMask, result);
		return CountExudatePixels(result);

	}


	int CalculateFalsePositive(Mat& groundTruth, Mat& exudateMask) {

		Mat result;
		subtract(exudateMask, groundTruth, result);
		return CountExudatePixels(result);

	}

	int CalculateTrueNegative(Mat& groundTruth, Mat& exudateMask) {
		Mat groundBack;
		inRange(groundTruth,0, 1, groundBack);

		Mat exudateBack;
		inRange(exudateMask,0, 1, exudateBack);

		Mat result;
		bitwise_and(groundBack, exudateBack, result);
		return CountExudatePixels(result);

	}

	int CalculateFalseNegative(Mat& groundTruth, Mat& exudateMask) {
		Mat result;
		subtract(groundTruth, exudateMask, result);
		return CountExudatePixels(result);

	}

	bool LoadGroundData(vector<Mat>& autoGroundData, vector<Mat>& semiGroundData) {


		string autoData[] = {"GroundTruthImages/auto/image001.png","GroundTruthImages/auto/image004.png","GroundTruthImages/auto/image005.png",
				"GroundTruthImages/auto/image006.png","GroundTruthImages/auto/image007.png","GroundTruthImages/auto/image012.png",
				"GroundTruthImages/auto/image013.png","GroundTruthImages/auto/image014.png","GroundTruthImages/auto/image015.png",
				"GroundTruthImages/auto/image016.png","GroundTruthImages/auto/image017.png","GroundTruthImages/auto/image019.png",
				"GroundTruthImages/auto/image025.png","GroundTruthImages/auto/image027.png","GroundTruthImages/auto/image066.png",
				"GroundTruthImages/auto/image068.png", "GroundTruthImages/auto/image088.png"
		};

		string semiData[] = {"GroundTruthImages/semi/image005.png","GroundTruthImages/semi/image006.png","GroundTruthImages/semi/image007.png",
				"GroundTruthImages/semi/image009.png","GroundTruthImages/semi/image012.png","GroundTruthImages/semi/image013.png",
				"GroundTruthImages/semi/image014.png","GroundTruthImages/semi/image015.png","GroundTruthImages/semi/image016.png",
				"GroundTruthImages/semi/image017.png","GroundTruthImages/semi/image019.png","GroundTruthImages/semi/image025.png"
		};

		vector<Mat> autoD;
		vector<Mat> semiD;

		for(int i = 0; i < 17; i++ ) {
			Mat loadedImage;
			loadedImage = imread(autoData[i], CV_LOAD_IMAGE_COLOR);
			if(!loadedImage.data) { //If the image can't be read
				cout << "Can't load: " << autoData[i] << endl;
				return false;
			}

			Size newSize((int)loadedImage.cols/3, (int)loadedImage.rows/3);
			resize(loadedImage, loadedImage,newSize, CV_INTER_CUBIC);
			autoD.push_back(loadedImage);

		}

		for(int i = 0; i < 12; i++ ) {
			Mat loadedImage;
			loadedImage = imread(semiData[i], CV_LOAD_IMAGE_COLOR);
			if(!loadedImage.data) { //If the image can't be read
				cout << "Can't load: " << semiData[i] << endl;
				return false;
			}

			Size newSize((int)loadedImage.cols/3, (int)loadedImage.rows/3);
			resize(loadedImage, loadedImage,newSize, CV_INTER_CUBIC);
			semiD.push_back(loadedImage);

		}

		autoGroundData = autoD;
		semiGroundData = semiD;

		return true;

	}


	void ExtractMarkedMask(Mat& mask, Mat& groundTruth) {


		Mat markMask;
		cvtColor(mask, markMask, CV_RGB2GRAY);
		inRange(markMask,200, 255, markMask);

		groundTruth = markMask;


	}

	void ExtractMarkedImages(vector<Mat>& images, vector<Mat>& masks, vector<Mat>& result){


		for(unsigned int i = 0 ; i < images.size(); i++) {

			Mat andImage;
			bitwise_and(images[i], images[i], andImage, masks[i]);
			result.push_back(andImage);

		}

	}

};
