/*
 * preprocess.h
 *
 *  Created on: May 10, 2015
 *      Author: nuttymtv1
 */

#ifndef PREPROCESS_H_
#define PREPROCESS_H_

class Preprocessor
{
public:

	Preprocessor(int colorMethod);
	void ImageCorrection(cv::Mat& image);
	void OpticalDiscSegmentation(cv::Mat& image, cv::Mat& mask, cv::Point2f& center);
	void VessleSegmentation (cv::Mat& image , cv::Mat& mask);
	void ExtractMinEnclosingCircle (cv::Mat& blobImage, cv::Mat& mask);


private:

	int colorSpaceMethod;

	void OpticalDiscMorphology(cv::Mat& image, cv::Mat& output, int dilateIter);
	void ExtractRetinaRegion(cv::Mat& image,cv::Mat& Retina, cv::Mat& mask);
	void SegmentOpticalCandidates(cv::Mat& image, cv::Mat& mask, cv::Mat& result, int erodeIter);
	void OpticalDiscExtraction(std::vector<std::vector<double > >& outputAreaVector, std::vector<cv::Point2f>& centroids, cv::Mat& image, cv::Mat& opticalDiskMask, cv::Mat& debugDrawing);
	cv::Point2f CalculateOpticalDiscCentroid(std::vector<cv::Point> contours);
	int SortOpticalDiscData(std::vector<std::vector<double> >& areaInfo,  std::vector<cv::Point2f>& centroids);


};



#endif /* PREPROCESS_H_ */
